//By David Lipps aka DaveTheDev @ EXPWorlds
//v2.0.0 for Godot 3.2

shader_type spatial;
render_mode ambient_light_disabled;

uniform vec4 base_color : hint_color = vec4(1.0);
uniform vec4 shade_color : hint_color = vec4(1.0);
uniform vec4 rim_tint : hint_color = vec4(0.75);

uniform float shade_threshold : hint_range(-1.0, 1.0, 0.001) = 0.0;
uniform float shade_softness : hint_range(0.0, 1.0, 0.001) = 0.01;

uniform bool use_shadow = false;
uniform float shadow_threshold : hint_range(0.00, 1.0, 0.001) = 0.7;
uniform float shadow_softness : hint_range(0.0, 1.0, 0.001) = 0.1;

uniform sampler2D base_texture: hint_albedo;
uniform sampler2D shade_texture: hint_albedo;

uniform vec4 emission_color : hint_color;
uniform sampler2D emission_map : hint_black_albedo;

void light() {
	float NdotL = dot(NORMAL, LIGHT);
	float is_lit = step(shade_threshold, NdotL);
	vec3 base = texture(base_texture, UV).rgb * base_color.rgb;
	vec3 shade = texture(shade_texture, UV).rgb * shade_color.rgb;
	vec3 diffuse = base;

	float shade_value = smoothstep(shade_threshold - shade_softness, shade_threshold + shade_softness, NdotL);
	diffuse = mix(shade, base, shade_value);
	
	float shadow_value = max(smoothstep(shadow_threshold - shadow_softness, shadow_threshold + shadow_softness, ATTENUATION.r), float(!use_shadow) * shade_value);
	shade_value = min(shade_value, shadow_value);
	diffuse = mix(shade, base, shade_value);
	is_lit = step(shadow_threshold, shade_value);
	
	vec3 result = diffuse * LIGHT_COLOR * ATTENUATION;
	DIFFUSE_LIGHT += result;
}

void fragment() {
	vec4 emission_tex = texture(emission_map, UV);
	vec4 base_tex = texture(base_texture, UV);
	EMISSION = emission_tex.rgb * emission_color.rgb * base_tex.rgb;
}
