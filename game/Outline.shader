shader_type spatial;
render_mode unshaded, cull_front;

uniform float outline_size = 0.1;
uniform vec4 outline_color : hint_color;

void vertex() {
	VERTEX += outline_size * NORMAL;
}

void fragment() {
	ALBEDO = outline_color.rgb;
	ALPHA = outline_color.a;
}
