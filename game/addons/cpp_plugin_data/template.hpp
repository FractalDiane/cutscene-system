{top-include-guard}

#include <Godot.hpp>
#include <{base-class}.hpp>

namespace godot {

class {class-name} : public {base-class} {
	GODOT_CLASS({class-name}, {base-class})

public:
	static void _register_methods();
	void _init();

	void _ready();
	void _process(float delta);
};{bottom-include-guard}

}
