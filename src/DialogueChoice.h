#pragma once

#include <Godot.hpp>
#include <Control.hpp>
#include <ResourceLoader.hpp>
#include <PackedScene.hpp>

namespace godot {

class DialogueChoice : public Control {
	GODOT_CLASS(DialogueChoice, Control)

private:
	Ref<PackedScene> choice_button = ResourceLoader::get_singleton()->load("res://Prefabs/ChoiceButton.tscn");

	Array buttons;

public:
	static void _register_methods();
	void _init();

	void create(Array choices);

private:
	void choice_clicked(int index);
	void _on_TimerDestroy_timeout();
};

}
