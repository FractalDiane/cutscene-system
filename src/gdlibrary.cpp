#include <Godot.hpp>

using namespace godot;

// Project includes
#include "Dialogue.h"
#include "DialogueChoice.h"
#include "Cutscene.h"
#include "CameraCrossfade.h"
#include "Controller.h"
#include "ChoiceButton.h"
#include "EnemyGroup.h"
#include "Battle.h"
#include "BattleCharacter.h"
#include "Character.h"
#include "BattleTurn.h"
#include "PartyManager.h"
#include "Player.h"
#include "YAML.h"
#include "Random.h"
#include "BattleUI.h"
#include "BattledialButton.h"
#include "Ability.h"


// godot_gdnative_init
extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options* o) {
	godot::Godot::gdnative_init(o);
}

// godot_gdnative_terminate
extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options* o) {
	godot::Godot::gdnative_terminate(o);
}

// godot_nativescript_init
extern "C" void GDN_EXPORT godot_nativescript_init(void* handle) {
	godot::Godot::nativescript_init(handle);

	register_class<Dialogue>();
	register_class<DialogueChoice>();
	register_class<Cutscene>();
	register_class<CameraCrossfade>();
	register_class<Controller>();
	register_class<ChoiceButton>();
	register_class<EnemyGroup>();
	register_class<Battle>();
	register_class<BattleCharacter>();
	register_class<Character>();
	register_class<BattleTurn>();
	register_class<PartyManager>();
	register_class<Player>();
	register_class<YAML>();
	register_class<Random>();
	register_class<BattleUI>();
	register_class<BattledialButton>();
	register_class<Ability>();

}
