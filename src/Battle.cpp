#include "Battle.h"

#include "BattleCharacter.h"
#include "PartyManager.h"
#include "GDMacros.h"
#include "Random.h"

#include <SceneTree.hpp>
#include <Viewport.hpp>
#include <Input.hpp>
#include <ResourceLoader.hpp>
#include <PackedScene.hpp>
#include <AnimationTree.hpp>
#include <AnimationNodeStateMachinePlayback.hpp>

using namespace godot;

namespace {
	Input* input;
	Random* _random;
}

void Battle::_register_methods() {
	REGISTER_PROPERTY_HINT(Battle, Ref<EnemyGroup>, enemy_group, Ref<EnemyGroup>(), GODOT_PROPERTY_HINT_RESOURCE_TYPE, "Resource");

	REGISTER_METHOD(Battle, _ready);
	REGISTER_METHOD(Battle, start_battle);
	REGISTER_METHOD(Battle, new_turn);
}

void Battle::_init() {}


void Battle::_ready() {
	input = Input::get_singleton();
	_random = Random::get_singleton();

	Ref<PackedScene> healthbar = ResourceLoader::get_singleton()->load(HealthbarPath);

	spawn_party(healthbar);
	spawn_enemies(healthbar);

	turn_manager = get_node<BattleTurn>("BattleTurn");
	turn_manager->initialize_turns(party, enemies);
	turn_manager->connect("all_turns_finished", this, "new_turn");
}


void Battle::start_battle() {
	turn_manager->run_next_turn();
}


void Battle::new_turn() {

}


void Battle::spawn_party(Ref<PackedScene> healthbar) {
	Array party_data = PartyManager::get_singleton()->get_party();
	Vector3 current_pos{2, 0, -2};

	for (int i = 0; i < party_data.size(); i++) {
		Ref<Character> this_char = cast_to<Character>(party_data[i]);
		spawn_character(this_char, current_pos, healthbar, party);
		current_pos.x -= 4;
	}
}


void Battle::spawn_enemies(Ref<PackedScene> healthbar) {
#ifdef GDN_DEBUG
	if (enemy_group.is_null()) {
		PRINT_ERROR("Enemy group field is empty");
		return;
	}
#endif

	Array enemies_data = enemy_group->get_enemies();
	Array enemy_positions = enemy_group->get_enemy_positions();

#ifdef GDN_DEBUG
	if (enemies_data.size() != enemy_positions.size()) {
		PRINT_ERROR(STRING_FORMAT("Mismatch in sizes of enemies array and positions array ({0} vs. {1})", enemies_data.size(), enemy_positions.size()));
		return;
	}
#endif

	for (int i = 0; i < enemies_data.size(); i++) {
		Ref<Character> this_enemy = cast_to<Character>(enemies_data[i]);
		Vector3 this_pos = enemy_positions[i];
		spawn_character(this_enemy, this_pos, healthbar, enemies);
	}
}


void Battle::spawn_character(Ref<Character> data, Vector3 position, Ref<PackedScene> healthbar, Array char_array) {
	BattleCharacter* new_char = BattleCharacter::_new();
	new_char->set_name("BattleCharacter");

	get_tree()->get_root()->get_node("Scene")->add_child(new_char);
	new_char->set_translation(position);
	new_char->spawn_character(data);

	Spatial* model = cast_to<Spatial>(new_char->get_child(0));
	model->get_node<AnimationTree>("AnimationTree")->set("parameters/Idle_Battle/seek/seek_position", _random->rand_range(0.0f, 2.0f));
	AnimationNodeStateMachinePlayback* playback = model->get_node<AnimationTree>("AnimationTree")->get("parameters/playback");
	playback->travel("Idle_Battle");

	if (data->get_visual_flip_180())
		model->set_rotation_degrees(Vector3(0, 180, 0));

	Spatial* healthbar_inst = cast_to<Spatial>(healthbar->instance());
	Spatial* hb_position = model->get_node<Spatial>("HealthbarPosition");
	healthbar_inst->set_translation(hb_position->get_translation());
	model->add_child(healthbar_inst);
	hb_position->queue_free();
	new_char->initialize_healthbar();

	char_array.push_back(new_char);
}
