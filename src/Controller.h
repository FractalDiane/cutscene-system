#pragma once

#include <Godot.hpp>
#include <Node.hpp>
#include <AudioStream.hpp>
#include <Tween.hpp>
#include <Timer.hpp>

namespace godot {

class Controller : public Node {
	GODOT_CLASS(Controller, Node)

private:
	static Controller* singleton;

	Dictionary flags = Dictionary::make(

	);

public:
	static void _register_methods();
	void _init();

	void _ready();
	void _process(float delta);

	void fade(float time, bool fadein, Color color = Color(0, 0, 0));

	int flag(String name);
	void set_flag(String name, int value);

	void play_sound_oneshot(Ref<AudioStream> sound, float volume = 0.0f, float pitch = 1.0f);
	void play_music(Ref<AudioStream> _music, float volume = 0.0f, float pitch = 1.0f);
	void fade_music(float time, bool fadein);

	Tween* construct_tween(bool cleanup_safeguard = true, float safeguard_time = 5.0f);
	Timer* construct_timer(float time, const Object* target, String method, Array binds = Array());

	static inline Controller* get_singleton() { return singleton; }
};

}
