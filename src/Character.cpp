#include "Character.h"

#include "GDMacros.h"

using namespace godot;

void Character::_register_methods() {
	ADD_EDITOR_GROUP(Character, "Stats", "stats_");
	REGISTER_PROPERTY(Character, stats_name, String());
	REGISTER_PROPERTY_HINT(Character, int, stats_max_hp, 1, GODOT_PROPERTY_HINT_RANGE, "0,999");
	REGISTER_PROPERTY_HINT(Character, int, stats_defense, 0, GODOT_PROPERTY_HINT_RANGE, "0,999");
	REGISTER_PROPERTY_HINT(Character, int, stats_agility, 2, GODOT_PROPERTY_HINT_RANGE, "0,999");

	ADD_EDITOR_GROUP(Character, "Abilities", "abilities_");
	REGISTER_PROPERTY_HINT(Character, Array, abilities_ability_list, Array(), GODOT_PROPERTY_HINT_TYPE_STRING, "17/17:Resource");

	ADD_EDITOR_GROUP(Character, "Visual", "visual_");
	REGISTER_PROPERTY_HINT(Character, Ref<PackedScene>, visual_model, Ref<PackedScene>(), GODOT_PROPERTY_HINT_RESOURCE_TYPE, "PackedScene");
	REGISTER_PROPERTY(Character, visual_flip_180, false);

	REGISTER_PROPERTY(Character, party_member, false);
}

void Character::_init() {}
