#include "EnemyGroup.h"

#include "GDMacros.h"

using namespace godot;

void EnemyGroup::_register_methods() {
	REGISTER_PROPERTY_HINT(EnemyGroup, Array, enemies, Array(), GODOT_PROPERTY_HINT_TYPE_STRING, "17/17:Resource");
	REGISTER_PROPERTY_HINT(EnemyGroup, Array, enemy_positions, Array(), GODOT_PROPERTY_HINT_TYPE_STRING, "7:");
}

void EnemyGroup::_init() {}
