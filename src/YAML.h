#pragma once

#include <Godot.hpp>
#include <Node.hpp>
#include <RegEx.hpp>

namespace godot {

class YAML : public Node {
	GODOT_CLASS(YAML, Node)

private:
	static YAML* singleton;
	static constexpr const char* LineRegexPat = R"(^(\s*)(.+):\s*(.*)$)";
	static constexpr const char* ListItemRegexPat = R"(^(\s*)-\s+(.+)$)";

	Ref<RegEx> line_regex = RegEx::_new();
	Ref<RegEx> list_item_regex = RegEx::_new();

public:
	static void _register_methods();
	void _init();

	void _ready();

	String encode(Dictionary object);
	Dictionary decode(String string);

	static inline YAML* get_singleton() { return singleton; }

private:
	void encode_all(Array out_lines, Dictionary object, unsigned int depth_level);
	Variant parse_value(String value);
};

}
