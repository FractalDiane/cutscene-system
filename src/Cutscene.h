#pragma once

#include "Dialogue.h"

#include <Godot.hpp>
#include <Node.hpp>
#include <Timer.hpp>

#include <RegEx.hpp>
#include <PackedScene.hpp>
#include <ResourceLoader.hpp>

namespace godot {

class Cutscene : public Node {
	GODOT_CLASS(Cutscene, Node)

private:
	enum Command {
		Null,
		Start,
		End,
		//Label,
		//Jump,
	
		DialogueText,
		DialogueChoice,
		
		CharacterAnimation,
		CharacterMove,
		CharacterTurn,
		
		Fade,
		CameraFade,
		CameraMove,
		
		PlaySound,
		PlayMusic,
		FadeMusic,
		
		DeclareRef,
		PlayEventAnimation,
		SetFlag,
		SetProperty,
		CallFunction,
		Wait,
	};

private:
	const Dictionary command_map = Dictionary::make(
		"NULL", Command::Null,
		"START", Command::Start,
		"END", Command::End,
		//"LABEL", Command::Label,
		//"JUMP", Command::Jump,
	
		"DIALOGUE", Command::DialogueText,
		"DIALOGUE_CHOICE", Command::DialogueChoice,
		
		"CHARACTER_ANIMATION", Command::CharacterAnimation,
		"CHARACTER_MOVE", Command::CharacterMove,
		"CHARACTER_TURN", Command::CharacterTurn,
		
		"FADE", Command::Fade,
		"CAMERA_FADE", Command::CameraFade,
		"CAMERA_MOVE", Command::CameraMove,
		
		"PLAY_SOUND", Command::PlaySound,
		"PLAY_MUSIC", Command::PlayMusic,
		"FADE_MUSIC", Command::FadeMusic,
		
		"DECLARE_REF", Command::DeclareRef,
		"PLAY_EVENT_ANIMATION", Command::PlayEventAnimation,
		"SET_FLAG", Command::SetFlag,
		"SET_PROPERTY", Command::SetProperty,
		"CALL_FUNCTION", Command::CallFunction,
		"WAIT", Command::Wait
	);

	static constexpr const char* CommandRegexPat = R"(^(\w+)\s+(.*)\(([\d\-]+),\s*([\d\-]+)\)\s+(.+)\s+(\{.*\})$)";
	static constexpr const char* DialogueRegexPat = R"(^\[(.+),\s*([\d\.\-]+)\]\s*(.+)$)";
	static constexpr const char* ChoiceRegexPat = R"(^\[(.+),\s*([\d\.\-]+)\]\s*(.+)\s*(\[.*\])$)";
	static constexpr const char* FunctionRegexPat = R"(^(.+)\s+(.+)\s+(\[.*\])$)";
	static constexpr float ShortWaitTime = 0.02f;

	Ref<RegEx> command_regex = RegEx::_new();
	Ref<RegEx> dialogue_regex = RegEx::_new();
	Ref<RegEx> choice_regex = RegEx::_new();
	Ref<RegEx> function_regex = RegEx::_new();

	Dictionary cutscene_table;
	String next_step;
	Dictionary declared_refs;

	String cutscene_file;
	Dictionary sounds_and_music;
	bool autostart = false;

	Ref<PackedScene> dialogue_ref = ResourceLoader::get_singleton()->load("res://Prefabs/Dialogue.tscn");
	Dialogue* dialogue;
	Ref<PackedScene> camera_crossfade_ref = ResourceLoader::get_singleton()->load("res://Prefabs/CameraCrossfade.tscn");

	Timer* timer_wait;

public:
	static void _register_methods();
	void _init();

	void _ready();
	
	void start_cutscene();

private:
	void generate_cutscene_table(String cutscene_file);
	void run_cutscene_step();
	void run_cutscene_step_choice(int choice_index);

	void generic_wait(float time);

	void _on_TimerWait_timeout();
};

}
