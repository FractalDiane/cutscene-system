#pragma once

#include <Godot.hpp>
#include <KinematicBody.hpp>
#include <Camera.hpp>
#include <AnimationTree.hpp>
#include <AnimationNodeStateMachinePlayback.hpp>

namespace godot {

class Player : public KinematicBody {
	GODOT_CLASS(Player, KinematicBody)

private:
	static constexpr float CameraMouseRotationSpeed = 0.001f;
	static constexpr float CameraControllerRotationSpeed = 3.0f;
	static constexpr float CameraXRotMin = -40.0f;
	static constexpr float CameraXRotMax = 30.0f;

	static constexpr float DirectionInterpolateSpeed = 1.0f;
	static constexpr float MotionInterpolateSpeed = 10.0f;
	static constexpr float RotationInterpolateSpeed = 10.0f;

	static constexpr float Speed = 4.0f;

	Vector3 player_model_base_scale;

	Transform orientation{};
	Transform root_motion{};
	Vector3 velocity{};
	Vector3 motion{};
	float camera_x_rot = 0.0f;

	Spatial* player_model;
	Spatial* camera_base;
	Spatial* camera_rot;
	Camera* camera_camera;
	AnimationTree* anim_tree;
	AnimationNodeStateMachinePlayback* anim_tree_playback;

public:
	static void _register_methods();
	void _init();

	void _ready();
	void _process(float delta);
	void _physics_process(float delta);
	void _input(InputEvent* event);

private:
	void rotate_camera(Vector2 move);
};

}
