#include "Player.h"

#include "Controller.h"
#include "GDMacros.h"

#include <Math.hpp>
#include <Input.hpp>
#include <InputEventMouseMotion.hpp>
#include <SceneTree.hpp>

using namespace godot;

namespace {
	Input* input;
	Controller* controller;
}

void Player::_register_methods() {
	REGISTER_METHOD(Player, _ready);
	REGISTER_METHOD(Player, _process);
	REGISTER_METHOD(Player, _physics_process);
	REGISTER_METHOD(Player, _input);
}

void Player::_init() {}


void Player::_ready() {
	player_model = get_node<Spatial>("Model");
	camera_base = get_node<Spatial>("CameraBase");
	camera_rot = get_node<Spatial>("CameraBase/CameraRot");
	camera_camera = get_node<Camera>("CameraBase/CameraRot/SpringArm/Camera");
	anim_tree = get_node<AnimationTree>("Model/AnimationTree");
	anim_tree_playback = anim_tree->get("parameters/playback");

	controller = Controller::get_singleton();
	input = Input::get_singleton();
	input->set_mouse_mode(Input::MOUSE_MODE_CAPTURED);

	player_model_base_scale = player_model->get_scale();
	orientation = player_model->get_global_transform();
	orientation.origin = Vector3();
}


void Player::_process(float delta) {
	if (input->is_action_just_pressed("sys_quit"))
		get_tree()->quit();
}


void Player::_physics_process(float delta) {
	// Camera movement
	Vector2 camera_move{
		input->get_action_strength("camera_right") - input->get_action_strength("camera_left"),
		input->get_action_strength("camera_up") - input->get_action_strength("camera_down")
	};

	float camera_speed = delta * CameraControllerRotationSpeed;
	rotate_camera(camera_move * camera_speed);

	// Player movement
	Transform cam_xform = camera_camera->get_global_transform();
	Vector3 motion_target{};
	motion_target += -cam_xform.basis[2] * input->get_action_strength("move_forward");
	motion_target += cam_xform.basis[2] * input->get_action_strength("move_back");
	motion_target += -cam_xform.basis[0] * input->get_action_strength("move_left");
	motion_target += cam_xform.basis[0] * input->get_action_strength("move_right");

	motion_target.y = 0;
	motion = motion.linear_interpolate(motion_target, MotionInterpolateSpeed * delta);

	move_and_slide(motion * Speed, Vector3(0, 1, 0));

	// Animate/rotate model
	if (motion_target != Vector3(0, 0, 0)) {
		float magnitude = motion_target.length();
		if (magnitude > 0.5f)
			anim_tree_playback->travel("Run");
		else {
			anim_tree->set("parameters/Walk/walk_speed/scale", magnitude / 0.5f);
			anim_tree_playback->travel("Walk");
		}

		Quat q_from = orientation.basis;
		Quat q_to{Vector3(0, 1, 0), static_cast<real_t>(atan2(motion_target.x, motion_target.z)) + Math::deg2rad(180.0f)};

		orientation.basis = Basis(q_from.slerp(q_to, delta * RotationInterpolateSpeed));
		Transform tf = player_model->get_global_transform();
		tf.basis = orientation.basis;
		player_model->set_global_transform(tf);

		Vector3 scale = player_model->get_scale();
		scale *= player_model_base_scale;
		player_model->set_scale(scale);
	}
	else {
		anim_tree_playback->travel("Idle");
	}
}


void Player::_input(InputEvent* event) {
	auto* ev = cast_to<InputEventMouseMotion>(event);
	if (ev) {
		rotate_camera(ev->get_relative() * CameraMouseRotationSpeed);
	}
}


void Player::rotate_camera(Vector2 move) {
	camera_base->rotate_y(-move.x);
	camera_base->orthonormalize();
	camera_x_rot += move.y;
	camera_x_rot = Math::clamp(camera_x_rot, Math::deg2rad(CameraXRotMin), Math::deg2rad(CameraXRotMax));
	
	Vector3 rot = camera_rot->get_rotation();
	rot.x = camera_x_rot;
	camera_rot->set_rotation(rot);
}
