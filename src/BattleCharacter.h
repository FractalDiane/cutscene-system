#pragma once

#include "Character.h"
#include "GDMacros.h"

#include <Godot.hpp>
#include <Spatial.hpp>
#include <TextureProgress.hpp>
#include <ResourceLoader.hpp>
#include <PackedScene.hpp>

namespace godot {

class BattleCharacter : public Spatial {
	GODOT_CLASS(BattleCharacter, Spatial)

private:
	Ref<PackedScene> healthbar_fragment = ResourceLoader::get_singleton()->load("res://Prefabs/HealthbarFragment.tscn");

	String character_name;
	int max_hp;
	int hp;
	int defense;
	int agility;
	bool party_member;
	Array ability_list = Array();

	Spatial* character_model;

public:
	static void _register_methods();
	void _init();

	void spawn_character(Ref<Character> character_data);
	void initialize_healthbar();

	void damage(int amount);

	GETTER(String, character_name);
	GETTER(int, max_hp);
	GETTER(int, hp);
	GETTER(int, defense);
	GETTER(int, agility);
	GETTER_BOOL(party_member);
	GETTER(Spatial*, character_model);
	GETTER(Array, ability_list);

	SETTER(bool, party_member);

private:
	void healthbar_animation(int old_health, TextureProgress* healthbar);
};

}
