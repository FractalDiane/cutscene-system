#include "YAML.h"

#include "GDMacros.h"

#include <RegExMatch.hpp>
#include <JSON.hpp>
#include <JSONParseResult.hpp>
#include <Math.hpp>

using namespace godot;

YAML* YAML::singleton = nullptr;

void YAML::_register_methods() {
	REGISTER_METHOD(YAML, _ready);
	REGISTER_METHOD(YAML, encode);
	REGISTER_METHOD(YAML, decode);
}

void YAML::_init() {}


void YAML::_ready() {
	if (!singleton)
		singleton = this;
	else
		queue_free();
	
	line_regex->compile(LineRegexPat);
	list_item_regex->compile(ListItemRegexPat);
}


Dictionary YAML::decode(String string) {
	Dictionary out_dict{};

	Array string_array = string.split("\n");
	Array stack{};
	Array key_stack{};
	int current_indentation = 0;

	for (int i = 0; i < string_array.size(); i++) {
		String this_line = string_array[i];
		auto regex_match = line_regex->search(this_line);
		auto regex_match_2 = list_item_regex->search(this_line);

		int line_indentation;
		if (!regex_match.is_null())
			line_indentation = regex_match->get_string(1).length();
		else if (!regex_match_2.is_null())
			line_indentation = regex_match_2->get_string(1).length();
		else
			line_indentation = 0;

		if (line_indentation < current_indentation) {
			int amount = std::abs(line_indentation - current_indentation) / 2;
			for (int i = 0; i < amount; i++) {
				if (stack.size() > 1) {
					if (stack[stack.size() - 2].get_type() == Variant::ARRAY) {
						Array arr = stack[stack.size() - 2];
						arr.push_back(stack.back());
					}
					else
						static_cast<Dictionary>(stack[stack.size() - 2])[key_stack.back()] = stack.back();
				}
				else
					out_dict[key_stack.back()] = stack.back();

				stack.pop_back();
				key_stack.pop_back();
			}
		}

		current_indentation = line_indentation;

		if (!regex_match.is_null()) {
			String line_key = regex_match->get_string(2);
			String line_value = regex_match->get_string(3);
			if (line_value.empty()) {
				String next_line = string_array[i + 1];
				String tmp = "-";
				if (next_line.strip_edges().begins_with(tmp))
					stack.push_back(Array());
				else
					stack.push_back(Dictionary());

				key_stack.push_back(parse_value(line_key));
			}
			else {
				if (stack.empty())
					out_dict[parse_value(line_key)] = line_value.strip_edges();
				else {
					if (stack.back().get_type() == Variant::ARRAY) {
						Array arr = stack.back();
						arr.push_back(parse_value(line_value.strip_edges()));
					}
					else
						static_cast<Dictionary>(stack.back())[parse_value(line_key)] = parse_value(line_value.strip_edges());
				}
			}
		}
		else if (!regex_match_2.is_null()) {
			String line_item = regex_match_2->get_string(2);
			
			Array arr = stack.back();
			arr.push_back(parse_value(line_item));
		}
	}

	while (!stack.empty()) {
		if (stack.size() > 1) {
			if (stack[stack.size() - 2].get_type() == Variant::ARRAY) {
				Array arr = stack[stack.size() - 2];
				arr.push_back(stack.back());
			}
			else
				static_cast<Dictionary>(stack[stack.size() - 2])[key_stack.back()] = stack.back();
		}
		else
			out_dict[key_stack.back()] = stack.back();

		stack.pop_back();
		key_stack.pop_back();
	}

	return out_dict;
}


Variant YAML::parse_value(String value) {
	// Int
	if (value.is_valid_integer())
		return Variant(value.to_int());
	
	// Float
	if (value.is_valid_float())
		return Variant(value.to_float());

	// Bool
	{
		String value_upper = value.to_upper();
		if (value_upper == "TRUE")
			return Variant(true);
		else if (value_upper == "FALSE")
			return Variant(false);
	}
	
	// List
	{
		String tmp = "[";
		if (value.begins_with(tmp)) {
			Array arr = JSON::get_singleton()->parse(value)->get_result();
			return Variant(arr);
		}
	}
	
	// Object
	{
		String tmp = "{";
		if (value.begins_with(tmp)) {
			Dictionary dict = JSON::get_singleton()->parse(value)->get_result();
			return Variant(dict);
		}
	}

	// String
	if (value[0] == '"') {
		value.erase(0, 1);
		value.erase(value.length() - 1, 1);
	}

	return Variant(value);
}


String YAML::encode(Dictionary object) {
	Array out_lines = Array::make("---");
	encode_all(out_lines, object, 0);
	String out_string{};
	for (int i = 0; i < out_lines.size(); i++) {
		if (i > 0)
			out_string += "\n";

		String this_line = out_lines[i];
		out_string += this_line;
	}

	return out_string;
}


void YAML::encode_all(Array out_lines, Dictionary object, unsigned int depth_level) {
	Array keys = object.keys();
	for (int i = 0; i < keys.size(); i++) {
		Variant this_item = object[keys[i]];

		String line{};
		for (int j = 0; j < depth_level; j++)
			line += "  ";

		switch (this_item.get_type()) {
			case Variant::ARRAY: {
				Array this_item_arr = this_item;
				if (this_item_arr.empty()) {
					line += STRING_FORMAT("{0}: []", keys[i]);
					out_lines.push_back(line);
				}
				else {
					line += STRING_FORMAT("{0}: ", keys[i]);
					out_lines.push_back(line);
					for (int j = 0; j < this_item_arr.size(); j++) {
						String list_line{};
						for (int k = 0; k < depth_level; k++)
							list_line += "  ";

						list_line += STRING_FORMAT("- {0}", this_item_arr[j]);
						out_lines.push_back(list_line);
					}
				}
			} break;

			case Variant::DICTIONARY: {
				Dictionary this_item_dict = this_item;
				if (this_item_dict.empty()) {
					line += STRING_FORMAT("{0}: {}", keys[i]);
					out_lines.push_back(line);
				}
				else {
					line += STRING_FORMAT("{0}:", keys[i]);
					out_lines.push_back(line);
					encode_all(out_lines, this_item_dict, depth_level + 1);
				}
			} break;

			default: {
				line += STRING_FORMAT("{0}: {1}", keys[i], this_item);
				out_lines.push_back(line);
			} break;
		}

		if (depth_level == 0)
			out_lines.push_back("");
	}
}
