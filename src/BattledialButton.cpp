#include "BattledialButton.h"

#include "GDMacros.h"

using namespace godot;

void BattledialButton::_register_methods() {
	REGISTER_METHOD(BattledialButton, _ready);
	REGISTER_METHOD(BattledialButton, _on_focus_grabbed);
	REGISTER_METHOD(BattledialButton, _on_focus_released);
	REGISTER_METHOD(BattledialButton, _on_BattledialButton_pressed);

	//REGISTER_PROPERTY(BattledialButton, command_name, String());
	REGISTER_PROPERTY(BattledialButton, index_offset, 0);

	register_signal<BattledialButton>("button_hover", "index", GODOT_VARIANT_TYPE_INT);
	register_signal<BattledialButton>("button_unhover");
	register_signal<BattledialButton>("button_press", "index", GODOT_VARIANT_TYPE_INT);
	register_signal<BattledialButton>("button_press_target", "target_index", GODOT_VARIANT_TYPE_INT);
}

void BattledialButton::_init() {}


void BattledialButton::_ready() {
	button_index = get_position_in_parent() + index_offset;
}


void BattledialButton::_on_focus_grabbed() {
	if (!is_disabled())
		emit_signal("button_hover", button_index);
	else
		release_focus();
}


void BattledialButton::_on_focus_released() {
	if (!is_disabled())
		emit_signal("button_unhover");
}


void BattledialButton::_on_BattledialButton_pressed() {
	if (!is_disabled()) {
		emit_signal("button_press", button_index);
		emit_signal("button_press_target", target_index);
	}
}
