#pragma once

#include <Godot.hpp>
#include <Control.hpp>
#include <AudioStream.hpp>
#include <DynamicFont.hpp>
#include <ResourceLoader.hpp>
#include <Camera.hpp>

#include "GDMacros.h"
#include "BattleCharacter.h"

namespace godot {

class BattleUI : public Control {
	GODOT_CLASS(BattleUI, Control)

private:
	Ref<DynamicFont> ui_font = ResourceLoader::get_singleton()->load("res://FontDialogue.tres");

	Ref<AudioStream> sound_cursor = ResourceLoader::get_singleton()->load("res://Audio/cursor.ogg"); 
	Ref<AudioStream> sound_cursor_2 = ResourceLoader::get_singleton()->load("res://Audio/Battle/UI/CursorBattle.ogg");
	Ref<AudioStream> sound_select = ResourceLoader::get_singleton()->load("res://Audio/select2.ogg");
	Ref<AudioStream> sound_select_2 = ResourceLoader::get_singleton()->load("res://Audio/Battle/UI/SelectBattle.ogg");

	Ref<PackedScene> menu_ability_ref = ResourceLoader::get_singleton()->load("res://Prefabs/BattleUI/MenuAbility.tscn");
	Ref<PackedScene> target_button_ref = ResourceLoader::get_singleton()->load("res://Prefabs/BattleUI/TargetButton.tscn");

	enum Menu {
		Attack,
		Magic,
		Items,
		Negotiate,
		Tactics
	};

	String command_text;
	bool show_command_text = false;
	int selected_option = 0;
	int selected_ability = 0;
	Control* selected_ability_obj;
	bool target_menu_open = false;
	bool casting = false;

	const String command_names[5] = {
		"Attack",
		"Magic",
		"Items",
		"Negotiate",
		"Tactics"
	};

	const Vector2 button_positions[5] = {
		Vector2(-6, -40),
		Vector2(138, -40),
		Vector2(-46, 96),
		Vector2(178, 96),
		Vector2(64, 190)
	};

	float text_wrap = 0;
	float text_diameter = 180.0f;

	int other_int;
	String third_value;
	String fourth_value;

	typedef void (BattleUI::*FuncPtr)();
	FuncPtr cancel_stack[5];
	int cancel_stack_pos = -1;

	const Vector2 TextPos = Vector2(312, 512);

	BattleCharacter* current_character;

public:
	static void _register_methods();
	void _init();

	void _ready();
	void _process(float delta);

	void show_battledial(bool show);
	void spawn_target_buttons(Array targets, Camera* camera);
	void flush_cancel_stack();

	GETTER(int, cancel_stack_pos);
	SETTER(BattleCharacter*, current_character);
	SETTER(bool, casting);

private:
	void spin_pentagram(bool spin, int selected_button);

	void show_menu_magic(bool show);
	void select_ability(bool select, int index);
	void clear_target_buttons();

	void menu_cancel_magic();
	void menu_cancel_target();

	inline void push_cancel_function(FuncPtr function) { if (cancel_stack_pos >= 4) { PRINT_ERROR("Cancel stack is full"); return; } cancel_stack[++cancel_stack_pos] = function; }
	inline void call_cancel_function() { (this->*cancel_stack[cancel_stack_pos--])(); }

	void button_hover(int index);
	void button_hover_2(int index);
	void button_unhover();

	void button_press_battledial(int index);
	void button_press_magic(int index);
	void button_press_target(int index);
};

}
