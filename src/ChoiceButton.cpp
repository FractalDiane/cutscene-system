#include "ChoiceButton.h"

#include "Controller.h"
#include "Random.h"
#include "GDMacros.h"

#include <Label.hpp>

using namespace godot;

namespace {
	Controller* controller;
	Random* _random;
}

void ChoiceButton::_register_methods() {
	REGISTER_METHOD(ChoiceButton, _ready);
	REGISTER_METHOD(ChoiceButton, _on_focus_grabbed);
	REGISTER_METHOD(ChoiceButton, _on_focus_released);
	REGISTER_METHOD(ChoiceButton, _on_ChoiceButton_pressed);
	REGISTER_METHOD(ChoiceButton, _on_TweenInvert_tween_all_completed);

	REGISTER_PROPERTY_HINT(ChoiceButton, Ref<AudioStream>, sound_hover, Ref<AudioStream>(), GODOT_PROPERTY_HINT_RESOURCE_TYPE, "AudioStream");
	REGISTER_PROPERTY_HINT(ChoiceButton, Ref<AudioStream>, sound_click, Ref<AudioStream>(), GODOT_PROPERTY_HINT_RESOURCE_TYPE, "AudioStream");
	
	register_signal<ChoiceButton>("button_pressed", GODOT_VARIANT_TYPE_INT, "index");
}

void ChoiceButton::_init() {}


void ChoiceButton::_ready() {
	controller = Controller::get_singleton();
	_random = Random::get_singleton();

	tween_invert = get_node<Tween>("TweenInvert");
	shader = get_material();

	if (!auto_grab)
		tween_invert->interpolate_property(*shader, "shader_param/invert_amount", 1.0f, 0.0f, 0.4f);
	else
		shader->set_shader_param("invert_amount", 1.0f);

	tween_invert->interpolate_property(this, "modulate", Color(1, 1, 1, 0), Color(1, 1, 1, 1), 0.4f);
	tween_invert->start();
}


void ChoiceButton::grab_focus_deferred() {
	auto_grab = true;
}


void ChoiceButton::set_choice_text(String text) {
	get_node<Label>("Text")->set_text(text);
}


void ChoiceButton::destroy() {
	button_ready = false;
	destroyed = true;
	tween_invert->interpolate_property(*shader, "shader_param/invert_amount", shader->get_shader_param("invert_amount"), 1.0f, 0.4f);
	tween_invert->interpolate_property(this, "modulate", get_modulate(), Color(1, 1, 1, 0), 0.4f);
	tween_invert->start();
}


void ChoiceButton::_on_focus_grabbed() {
	if (button_ready) {
		if (!auto_grab) {
			controller->play_sound_oneshot(sound_hover, 0.0f, _random->rand_range(0.95f, 1.05f));
			tween_invert->interpolate_property(*shader, "shader_param/invert_amount", shader->get_shader_param("invert_amount"), 1.0f, 0.4f);
			tween_invert->start();
		}
	}
	else
		release_focus();
}


void ChoiceButton::_on_focus_released() {
	if (button_ready) {
		tween_invert->interpolate_property(*shader, "shader_param/invert_amount", shader->get_shader_param("invert_amount"), 0.0f, 0.4f);
		tween_invert->start();
	}
}


void ChoiceButton::_on_ChoiceButton_pressed() {
	if (button_ready) {
		controller->play_sound_oneshot(sound_click, 6.0f, _random->rand_range(0.98f, 1.05f));
		emit_signal("button_pressed", get_position_in_parent());
	}
}


void ChoiceButton::_on_TweenInvert_tween_all_completed() {
	if (!button_ready && !destroyed) {
		button_ready = true;
		if (auto_grab) {
			grab_focus();
			auto_grab = false;
		}
	}
}
