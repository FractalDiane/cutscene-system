#include "BattleTurn.h"

//#include "PartyManager.h"
#include "CameraCrossfade.h"
#include "Random.h"
#include "Controller.h"

#include <Spatial.hpp>
#include <Particles.hpp>
#include <Input.hpp>
#include <SceneTree.hpp>
#include <Viewport.hpp>
#include <Timer.hpp>
#include <AnimationPlayer.hpp>
#include <Tween.hpp>
#include <Animation.hpp>
#include <AnimationTree.hpp>
#include <AnimationNodeStateMachinePlayback.hpp>

#define GET_RANDOM_CAMERA(battlecharacter, root_node) cast_to<Camera>(_random->random_item(battlecharacter->get_character_model()->get_node<Spatial>(root_node)->get_children()))

using namespace godot;

namespace {
	Random* _random;
	Controller* controller;
}

void BattleTurn::_register_methods() {
	REGISTER_METHOD(BattleTurn, _ready);

	REGISTER_METHOD(BattleTurn, use_ability);
	REGISTER_METHOD(BattleTurn, show_cast_particles);
	REGISTER_METHOD(BattleTurn, damage);
	REGISTER_METHOD(BattleTurn, run_next_turn);
	
	REGISTER_METHOD(BattleTurn, target_menu_opened);
	REGISTER_METHOD(BattleTurn, target_menu_closed);
	REGISTER_METHOD(BattleTurn, target_button_clicked);

	REGISTER_METHOD(BattleTurn, _on_TimerChangeCamera_timeout);

	register_signal<BattleTurn>("all_turns_finished");
}

void BattleTurn::_init() {}


void BattleTurn::_ready() {
	_random = Random::get_singleton();
	controller = Controller::get_singleton();
	
	current_camera = get_node<Camera>("../CameraStart");
	battle_ui = get_node<BattleUI>("../UI/BattleUI");
	battle_ui->connect("target_menu_opened", this, NAMEOF(target_menu_opened));
	battle_ui->connect("target_menu_closed", this, NAMEOF(target_menu_closed));
	battle_ui->connect("target_button_clicked", this, NAMEOF(target_button_clicked));
}


void BattleTurn::initialize_turns(Array party, Array enemies) {
	for (int i = 0; i < party.size(); i++) {
		BattleCharacter* this_character = cast_to<BattleCharacter>(party[i]);	
		turn_queue.push(CharacterPriority(this_character, this_character->get_agility()));
	}

	for (int i = 0; i < enemies.size(); i++) {
		BattleCharacter* this_enemy = cast_to<BattleCharacter>(enemies[i]);
		turn_queue.push(CharacterPriority(this_enemy, this_enemy->get_agility()));
	}

	this->party = party;
	this->enemies = enemies;
}


void BattleTurn::run_next_turn() {
	battle_ui->set_casting(false);
	if (!turn_queue.empty()) {
		current_character = turn_queue.top().character;
		battle_ui->set_current_character(current_character);
		Godot::print("{0}'s turn", current_character->get_character_name());
		if (current_character->is_party_member()) {
			Camera* target_camera = GET_RANDOM_CAMERA(current_character, "BattleCameras");
			fade_to_camera(target_camera);
			battle_ui->show_battledial(true);
			get_node<Timer>("../TimerChangeCamera")->start();
		}
		else {

		}

		turn_queue.pop();
	}
	else {
		emit_signal("all_turns_finished");
	}
}


void BattleTurn::use_ability(Ref<Ability> ability, Ref<PackedScene> animation, BattleCharacter* target) {
	Camera* target_camera = GET_RANDOM_CAMERA(target, "CastCameras");
	tween_to_camera(target_camera, 1.6f, true);

	Spatial* anim = cast_to<Spatial>(animation->instance());
	get_tree()->get_root()->add_child(anim);
	Transform tf = anim->get_global_transform();
	tf.origin += target->get_global_transform().origin;
	anim->set_global_transform(tf);
	//anim->set_global_transform(target->get_global_transform());
	

	AnimationPlayer* player = anim->get_node<AnimationPlayer>("AnimationPlayer");
	anim->connect("renamed", this, "damage", Array::make(target, 3), CONNECT_ONESHOT);
	
	controller->construct_timer(1.2f, player, "play", Array::make(String("Animation")));
	controller->construct_timer(player->get_animation("Animation")->get_length(), this, "run_next_turn");
}


void BattleTurn::damage(BattleCharacter* target, int amount) {
	target->damage(amount);
}


void BattleTurn::fade_to_camera(Camera* target, bool stop_timer) {
	if (stop_timer)
		get_node<Timer>("../TimerChangeCamera")->stop();

	CameraCrossfade* crossfade = cast_to<CameraCrossfade>(camera_crossfade_ref->instance());
	target->get_parent()->add_child(crossfade);
	crossfade->crossfade(current_camera, target, CameraFadeTime);
	current_camera = target;
}


void BattleTurn::tween_to_camera(Camera* target, float time, bool stop_timer) {
	if (stop_timer)
		get_node<Timer>("../TimerChangeCamera")->stop();

	Camera* camera_clone = Camera::_new();
	camera_clone->set_global_transform(current_camera->get_global_transform());
	current_camera->set_current(false);
	camera_clone->set_current(true);
	get_tree()->get_root()->add_child(camera_clone);

	Vector3 rot = target->get_global_transform().basis.get_euler();
	Transform xform = Transform();
	xform.translate(target->get_global_transform().origin);
	xform.rotate_basis(Vector3(1, 0, 0), rot.x);
	xform.rotate_basis(Vector3(0, 1, 0), rot.y);
	xform.rotate_basis(Vector3(0, 0, 1), rot.z);
	xform.orthonormalize();

	current_camera = target;
	Tween* tween_camera = controller->construct_tween();
	tween_camera->interpolate_property(camera_clone, "transform", camera_clone->get_transform(), xform, time, Tween::TRANS_CUBIC, Tween::EASE_IN_OUT);
	tween_camera->connect("tween_all_completed", target, "set_current", Array::make(true));
	tween_camera->connect("tween_all_completed", camera_clone, "queue_free");
	tween_camera->start();
}


void BattleTurn::show_cast_particles() {
	Particles* parts = cast_to<Particles>(current_character->is_party_member() ? particles_cast->instance() : particles_cast_2->instance());
	Spatial* character = current_character->get_character_model();
	Vector3 char_translation = character->get_global_transform().origin;
	parts->set_translation(Vector3(char_translation.x, -0.29f, char_translation.z));
	get_tree()->get_root()->add_child(parts);
	parts->set_emitting(true);
}


void BattleTurn::target_menu_opened(bool all_enemies) {
	tween_to_camera(get_node<Camera>("../CameraTarget"), 1.0f, true);
	battle_ui->spawn_target_buttons(enemies, current_camera);
}


void BattleTurn::target_menu_closed() {
	tween_to_camera(GET_RANDOM_CAMERA(current_character, "BattleCameras"), 1.0f, false);
	get_node<Timer>("../TimerChangeCamera")->start();
}


void BattleTurn::target_button_clicked(int index, int ability_index) {
	battle_ui->show_battledial(false);
	battle_ui->flush_cancel_stack();

	Ref<Ability> ability = cast_to<Ability>(current_character->get_ability_list()[ability_index]);
	Camera* target_camera = GET_RANDOM_CAMERA(current_character, "CastCameras");
	tween_to_camera(target_camera, 1.6f, true);
	
	auto* tree = current_character->get_character_model()->get_node<AnimationTree>("AnimationTree");
	AnimationNodeStateMachinePlayback* playback = tree->get("parameters/playback");
	playback->travel("Cast");

	controller->construct_timer(1.2f, this, "show_cast_particles");
	controller->construct_timer(2.8f, this, "use_ability", Array::make(ability, ability->get_visual_animation(), cast_to<BattleCharacter>(enemies[index])));
}


void BattleTurn::_on_TimerChangeCamera_timeout() {
	switch (_random->rand_range(0, 1)) {
		case 0: {
			Camera* target_camera = get_node<Camera>("../CameraPivot/Camera2");
			if (target_camera != current_camera)
				get_node<AnimationPlayer>("../AnimationPlayerCamera")->seek(_random->rand_range(0.0f, 15.0f), true);
			
			fade_to_camera(target_camera);
		} break;

		case 1: {
			Camera* target_camera = GET_RANDOM_CAMERA(current_character, "BattleCameras");
			fade_to_camera(target_camera);
		} break;
	}

	get_node<Timer>("../TimerChangeCamera")->start();
}
