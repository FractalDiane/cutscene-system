#pragma once

#include "GDMacros.h"

#include <Godot.hpp>
#include <Node.hpp>

namespace godot {

class PartyManager : public Node {
	GODOT_CLASS(PartyManager, Node)

private:
	static PartyManager* singleton;
	
	Array party = Array();

public:
	static void _register_methods();
	void _init();

	void _ready();

	GETTER(Array, party);

	static inline PartyManager* get_singleton() { return singleton; }
};

}
