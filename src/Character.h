#pragma once

#include "GDMacros.h"

#include <Godot.hpp>
#include <Resource.hpp>
#include <PackedScene.hpp>

namespace godot {

class Character : public Resource {
	GODOT_CLASS(Character, Resource)

protected:
	String stats_name;
	int stats_max_hp;
	int stats_defense;
	int stats_agility;
	Ref<PackedScene> visual_model;
	bool visual_flip_180 = false;
	bool party_member = false;
	Array abilities_ability_list = Array();

public:
	static void _register_methods();
	void _init();

	GETTER(String, stats_name);
	GETTER(int, stats_max_hp);
	GETTER(int, stats_defense);
	GETTER(int, stats_agility);
	GETTER(Ref<PackedScene>, visual_model);
	GETTER(bool, visual_flip_180);
	GETTER_BOOL(party_member);
	GETTER_NAME(Array, abilities_ability_list, ability_list);
};

}
