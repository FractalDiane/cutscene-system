#include "BattleUI.h"

#include "BattledialButton.h"
#include "Controller.h"
#include "GDMacros.h"
#include "Ability.h"
#include "Random.h"
#include "BattleCharacter.h"

#include <Tween.hpp>
#include <Input.hpp>
#include <BaseButton.hpp>
#include <Label.hpp>

using namespace godot;

namespace {
	Controller* controller;
	Input* input;
	Random* _random;
}

void BattleUI::_register_methods() {
	REGISTER_METHOD(BattleUI, _ready);
	REGISTER_METHOD(BattleUI, _process);

	REGISTER_METHOD(BattleUI, button_hover);
	REGISTER_METHOD(BattleUI, button_hover_2);
	REGISTER_METHOD(BattleUI, button_unhover);

	REGISTER_METHOD(BattleUI, button_press_battledial);
	REGISTER_METHOD(BattleUI, button_press_magic);
	REGISTER_METHOD(BattleUI, button_press_target);

	REGISTER_PROPERTY(BattleUI, text_diameter, 180.0f);
	REGISTER_PROPERTY_HINT(BattleUI, int, other_int, 24, GODOT_PROPERTY_HINT_RANGE, "0,99");
	REGISTER_PROPERTY_HINT(BattleUI, String, third_value, "Hello", GODOT_PROPERTY_HINT_MULTILINE_TEXT, "");
	REGISTER_PROPERTY_HINT(BattleUI, String, fourth_value, "There", GODOT_PROPERTY_HINT_MULTILINE_TEXT, "");

	register_signal<BattleUI>("target_menu_opened", "all_enemies", GODOT_VARIANT_TYPE_BOOL);
	register_signal<BattleUI>("target_menu_closed");
	register_signal<BattleUI>("target_button_clicked", "index", GODOT_VARIANT_TYPE_INT, "ability", GODOT_VARIANT_TYPE_INT);
}

void BattleUI::_init() {}


void BattleUI::_ready() {
	controller = Controller::get_singleton();
	input = Input::get_singleton();
	_random = Random::get_singleton();

	Control* battledial = get_node<Control>("Battledial");
	for (int i = 1; i <= 5; i++) {
		Control* button = cast_to<Control>(battledial->get_child(i));
		button->connect("button_hover", this, "button_hover");
		button->connect("button_unhover", this, "button_unhover");
		button->connect("button_press", this, NAMEOF(button_press_battledial));
	}

	Control* ability_menu = get_node<Control>("Battledial/AbilityMenu");
	ability_menu->set_position(Vector2(94, 120));
	ability_menu->set_scale(Vector2(0, 0));
	ability_menu->set_modulate(Color(1, 1, 1, 0));

	battledial->set_modulate(Color(1, 1, 1, 0));
}


void BattleUI::_process(float delta) {
	text_wrap += delta * 15;

	if (input->is_action_just_pressed("ui_cancel") && cancel_stack_pos > -1)
		call_cancel_function();
}


void BattleUI::show_battledial(bool show) {
	Tween* tween = controller->construct_tween();
	tween->interpolate_property(get_node<Control>("Battledial"), "modulate", Color(1, 1, 1, show ? 0 : 1), Color(1, 1, 1, show ? 1 : 0), 0.5f, 0, 2, 0.4f);
	tween->start();
}


void BattleUI::spawn_target_buttons(Array targets, Camera* camera) {
	for (int i = 0; i < targets.size(); i++) {
		BattleCharacter* this_char = cast_to<BattleCharacter>(targets[i]);
		Control* inst = cast_to<Control>(target_button_ref->instance());
		inst->set_position(camera->unproject_position(this_char->get_character_model()->get_global_transform().origin));
		BattledialButton* but = inst->get_node<BattledialButton>("Button");
		but->connect("button_hover", this, "button_hover_2");
		but->connect("button_press_target", this, NAMEOF(button_press_target));
		but->set_target_index(i);
		get_node<Control>("TargetButtons")->add_child(inst);
	}
}


void BattleUI::flush_cancel_stack() {
	while (cancel_stack_pos > -1)
		call_cancel_function();
}


void BattleUI::spin_pentagram(bool spin, int selected_button) {
	if (!casting)
		controller->play_sound_oneshot(sound_select_2);

	Tween* tween = controller->construct_tween();
	Control* battledial = get_node<Control>("Battledial");
	Control* pentagram = get_node<Control>("Battledial/Pentagram");

	tween->interpolate_property(pentagram, "rect_rotation", pentagram->get_rotation_degrees(), spin ? 35 : 0, 1.0f, Tween::TRANS_QUINT, Tween::EASE_OUT);
	tween->interpolate_property(battledial, "rect_position", battledial->get_position(), spin ? Vector2(100, 392) : Vector2(184, 392), 1.0f, Tween::TRANS_QUINT, Tween::EASE_OUT);
	tween->start();

	Tween* tween2 = controller->construct_tween();
	for (int i = 1; i <= 5; i++) {
		BaseButton* this_child = cast_to<BaseButton>(battledial->get_child(i));
		this_child->set_disabled(spin);
		if (i - 1 == selected_button) {
			tween2->interpolate_property(this_child, "rect_position", this_child->get_position(), spin ? Vector2(64, 64) : button_positions[selected_option], 0.75f, Tween::TRANS_QUINT, Tween::EASE_OUT);
			float duration = 0.1f;
			tween2->interpolate_property(this_child, "rect_scale",  Vector2(0.5f, 0.5f), Vector2(0, 0.5f), duration, 0, 2, 0.0f);
			tween2->interpolate_property(this_child, "rect_scale", Vector2(0, 0.5f),  Vector2(0.5f, 0.5f), duration * 4, Tween::TRANS_QUINT, Tween::EASE_OUT, duration);
		}
		else {
			tween2->interpolate_property(this_child, "self_modulate", Color(1, 1, 1, spin ? 1 : 0), Color(1, 1, 1, spin ? 0 : 1), 0.75f);
		}
	}

	tween2->start();
}


void BattleUI::show_menu_magic(bool show) {
	Control* menu = get_node<Control>("Battledial/AbilityMenu");
	Array abilities = current_character->get_ability_list();
	for (int i = 0; i < menu->get_child(0)->get_child_count(); i++)
		menu->get_child(0)->get_child(i)->queue_free();

	for (int i = 0; i < abilities.size(); i++) {
		Ref<Ability> this_ability = cast_to<Ability>(abilities[i]);
		
		Control* inst = cast_to<Control>(menu_ability_ref->instance());
		BaseButton* but = inst->get_node<BaseButton>("Button");
		but->set_disabled(!show);
		but->connect("button_hover", this, "button_hover_2");
		but->connect("button_press", this, NAMEOF(button_press_magic));

		inst->get_node<Label>("Name")->set_text(this_ability->get_ability_name());
		inst->get_node<Label>("Cost")->set_text(STRING_FORMAT("{0} EP", this_ability->get_ability_cost()));
		menu->get_child(0)->add_child(inst);
	}

	Tween* tween = controller->construct_tween();
	tween->interpolate_property(menu, "rect_scale", menu->get_scale(), Vector2(show ? 1 : 0, show ? 1 : 0), 1.0f, Tween::TRANS_QUINT, Tween::EASE_OUT);
	tween->interpolate_property(menu, "rect_position", menu->get_position(), show ? Vector2(288, 0) : Vector2(94, 120), 1.0f, Tween::TRANS_QUINT, Tween::EASE_OUT);
	tween->interpolate_property(menu, "modulate", menu->get_modulate(), Color(1, 1, 1, show ? 1 : 0), 1.0f, Tween::TRANS_QUINT, Tween::EASE_OUT);
	tween->start();
}


void BattleUI::select_ability(bool select, int index) {
	Control* menu = get_node<Control>("Battledial/AbilityMenu");
	Array abilities = current_character->get_ability_list();

	if (!select) {
		remove_child(selected_ability_obj);
		menu->get_child(0)->add_child(selected_ability_obj);
		menu->get_child(0)->move_child(selected_ability_obj, selected_ability);
		selected_ability_obj = nullptr;
	}

	for (int i = 0; i < abilities.size(); i++) {
		Control* this_child = cast_to<Control>(menu->get_child(0)->get_child(i));
		Tween* tween = controller->construct_tween();

		if (i == index) {
			if (select) {
				Vector2 pos = this_child->get_global_position();
				this_child->get_parent()->remove_child(this_child);
				add_child(this_child);
				this_child->set_global_position(pos);
				selected_ability_obj = this_child;
			}

			this_child->get_node<Control>("HSeparator")->set_visible(!select);
			tween->interpolate_property(this_child, "rect_position", select ? this_child->get_global_position() : Vector2(0, 80), select ? Vector2(this_child->get_global_position().x, 600) : Vector2(0, 0), 1.0f, Tween::TRANS_QUINT, Tween::EASE_OUT);
		}
		else {
			tween->interpolate_property(this_child, "modulate", Color(1, 1, 1, select ? 1 : 0), Color(1, 1, 1, select ? 0 : 1), 1.0f);
		}

		this_child->get_node<BaseButton>("Button")->set_disabled(select);
		
		tween->start();
	}

	Tween* tween = controller->construct_tween();
	tween->interpolate_property(menu, "rect_size", Vector2(448, select ? 260 : 50), Vector2(448, select ? 50 : 260), 1.0f, Tween::TRANS_QUINT, Tween::EASE_OUT);
	tween->interpolate_property(menu, "rect_position", menu->get_position(), menu->get_position() + Vector2(0, select ? 210 : -210), 1.0f, Tween::TRANS_QUINT, Tween::EASE_OUT);
	tween->start();
}


void BattleUI::clear_target_buttons() {
	Control* root = get_node<Control>("TargetButtons");
	for (int i = 0; i < root->get_child_count(); i++) {
		root->get_child(i)->queue_free();
	}
}


// ==========================================================================================

void BattleUI::menu_cancel_magic() {
	show_menu_magic(false);
	spin_pentagram(false, selected_option);
}


void BattleUI::menu_cancel_target() {
	switch (selected_option) {
		case Menu::Attack:
		case Menu::Negotiate:
			spin_pentagram(false, selected_option);
			break;

		default:
			select_ability(false, selected_ability);
			break;
	}

	clear_target_buttons();
	if (!casting) {
		controller->play_sound_oneshot(sound_select);
		emit_signal("target_menu_closed");
	}
}

// ==========================================================================================

void BattleUI::button_hover(int index) {
	controller->play_sound_oneshot(sound_cursor_2);
	command_text = command_names[index];
	show_command_text = true;
}


void BattleUI::button_hover_2(int index) {
	controller->play_sound_oneshot(sound_cursor, 0.0f, _random->rand_range(0.95f, 1.05f));
}


void BattleUI::button_unhover() {
	show_command_text = false;
}

// ==========================================================================================

void BattleUI::button_press_battledial(int index) {
	Tween* tween = controller->construct_tween();
	tween->interpolate_property(this, "text_diameter", 180.0f, 145.0f, 1.0f, Tween::TRANS_QUINT, Tween::EASE_OUT);
	tween->start();

	selected_option = index;
	spin_pentagram(true, index);

	switch (index) {
		case Menu::Attack:
			push_cancel_function(&BattleUI::menu_cancel_target);
			emit_signal("target_menu_opened", false);
			break;

		case Menu::Magic:
			show_menu_magic(true);
			push_cancel_function(&BattleUI::menu_cancel_magic);
			break;
	}
}


void BattleUI::button_press_magic(int index) {
	controller->play_sound_oneshot(sound_select);

	select_ability(true, index);
	selected_ability = index;

	push_cancel_function(&BattleUI::menu_cancel_target);
	emit_signal("target_menu_opened", false);
}


void BattleUI::button_press_target(int index) {
	controller->play_sound_oneshot(sound_select_2);
	casting = true;
	clear_target_buttons();
	emit_signal("target_button_clicked", index, selected_ability);
}
