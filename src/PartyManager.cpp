#include "PartyManager.h"

#include "GDMacros.h"
#include "Character.h"

#include <Resource.hpp>
#include <ResourceLoader.hpp>

using namespace godot;

PartyManager* PartyManager::singleton = nullptr;

void PartyManager::_register_methods() {
	REGISTER_METHOD(PartyManager, _ready);

	REGISTER_PROPERTY_HINT(PartyManager, Array, party, Array(), GODOT_PROPERTY_HINT_TYPE_STRING, "17:");
}

void PartyManager::_init() {}


void PartyManager::_ready() {
	if (!singleton)
		singleton = this;
	else
		queue_free();
}
