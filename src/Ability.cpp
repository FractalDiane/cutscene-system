#include "Ability.h"

#include "GDMacros.h"

using namespace godot;

void Ability::_register_methods() {
	REGISTER_PROPERTY(Ability, ability_name, String());
	REGISTER_PROPERTY_HINT(Ability, int, ability_element, 0, GODOT_PROPERTY_HINT_ENUM, "Flame,Shadow,Blight,Chaos");
	REGISTER_PROPERTY_HINT(Ability, int, ability_type, 0, GODOT_PROPERTY_HINT_ENUM, "Change Health,Change Stat");
	REGISTER_PROPERTY(Ability, ability_cost, 0);
	
	ADD_EDITOR_GROUP(Ability, "Health", "health_");
	REGISTER_PROPERTY(Ability, health_heal, false);
	REGISTER_PROPERTY(Ability, health_amount, 0);

	ADD_EDITOR_GROUP(Ability, "Stat", "stat_");
	REGISTER_PROPERTY_HINT(Ability, int, stat_affected_stat, 0, GODOT_PROPERTY_HINT_ENUM, "Attack,Defense,Agility,Evasion");

	ADD_EDITOR_GROUP(Ability, "Visual", "visual_");
	REGISTER_PROPERTY_HINT(Ability, Ref<PackedScene>, visual_animation, Ref<PackedScene>(), GODOT_PROPERTY_HINT_RESOURCE_TYPE, "PackedScene");
}

void Ability::_init() {}
