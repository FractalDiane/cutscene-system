// GDMacros.h
#pragma once

#ifdef _MSC_VER
#define __ALWAYS_INLINE__ __forceinline inline
#else
#define __ALWAYS_INLINE__ __attribute__((always_inline)) inline
#endif

//#define FOREACH(type, var, target) for (int i = 0, type var = target[0]; i < target.size(); i++, var = target[i])

#define NAMEOF(object) #object

#define PRINT_WARNING(message) godot::Godot::print_warning(message, __func__, __FILE__, __LINE__)
#define PRINT_ERROR(message) godot::Godot::print_error(message, __func__, __FILE__, __LINE__)

#define STRING_FORMAT(fmt, ...) String(fmt).format(Array::make(__VA_ARGS__))

#define REGISTER_METHOD(class_name, method) register_method(#method, &class_name::method)
#define REGISTER_PROPERTY(class_name, var, _default) register_property(#var, &class_name::var, _default)
#define REGISTER_PROPERTY_HINT(class_name, type, var, _default, hint, hint_string) register_property<class_name, type>(#var, &class_name::var, _default, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_DEFAULT, hint, hint_string)
#define ADD_EDITOR_GROUP(class_name, group_name, group_prefix)    \
	godot::register_property<class_name, godot::Variant>(         \
		group_name,                                               \
		nullptr,                                                  \
		godot::Variant::NIL,                                      \
		GODOT_METHOD_RPC_MODE_DISABLED,                           \
		GODOT_PROPERTY_USAGE_GROUP,                               \
		GODOT_PROPERTY_HINT_NONE,                                 \
		group_prefix)

#define GETTER(type, var) inline type get_##var() { return var; }
#define GETTER_NAME(type, var, name) inline type get_##name() { return var; }
#define GETTER_BOOL(var) inline bool is_##var() { return var; }
#define SETTER(type, var) inline void set_##var(type value) { var = value; }

enum {
	GODOT_MARGIN_LEFT = 0,
	GODOT_MARGIN_TOP = 1,
	GODOT_MARGIN_RIGHT = 2,
	GODOT_MARGIN_BOTTOM = 3
};
