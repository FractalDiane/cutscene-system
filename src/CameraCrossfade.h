#pragma once

#include <Godot.hpp>
#include <ViewportContainer.hpp>
#include <Camera.hpp>

namespace godot {

class CameraCrossfade : public ViewportContainer {
GODOT_CLASS(CameraCrossfade, ViewportContainer)

private:
	Camera* old_camera_ref;
	Camera* camera_clone;

public:
	static void _register_methods();
	void _init();

	void _process(float delta);

	void crossfade(Camera* old_camera, Camera* new_camera, float time);

private:
	void _on_TweenFade_tween_all_completed();
};

}
