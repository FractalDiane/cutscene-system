#include "BattleCharacter.h"

#include "GDMacros.h"
#include "Random.h"

#include <Math.hpp>
#include <TextureRect.hpp>
#include <AtlasTexture.hpp>
#include <Tween.hpp>
#include <AnimationTree.hpp>
#include <AnimationNodeStateMachinePlayback.hpp>

#define GET_HEALTHBAR() character_model->get_node<TextureProgress>("Healthbar3D/Viewport/Healthbar/CenterContainer/Bar")

using namespace godot;

void BattleCharacter::_register_methods() {
	register_signal<BattleCharacter>("damage_animation_finished");
}

void BattleCharacter::_init() {}


void BattleCharacter::spawn_character(Ref<Character> enemy_data) {
	Spatial* model = cast_to<Spatial>(enemy_data->get_visual_model()->instance());
	add_child(model);
	character_model = model;

	character_name = enemy_data->get_stats_name();
	max_hp = enemy_data->get_stats_max_hp();
	hp = max_hp;
	defense = enemy_data->get_stats_defense();
	agility = enemy_data->get_stats_agility();
	party_member = enemy_data->is_party_member();
	ability_list = enemy_data->get_ability_list();
}


void BattleCharacter::initialize_healthbar() {
	TextureProgress* healthbar = GET_HEALTHBAR();
	healthbar->set_max(max_hp);
	healthbar->set_value(max_hp);
	healthbar->hide();
}


void BattleCharacter::damage(int amount) {
	TextureProgress* healthbar = GET_HEALTHBAR();
	healthbar->show();

	int old_health = hp;
	hp = Math::clamp(old_health - amount, 0, max_hp);
	healthbar->set_value(hp);

	AnimationTree* tree = character_model->get_node<AnimationTree>("AnimationTree");
	AnimationNodeStateMachinePlayback* playback = tree->get("parameters/playback");
	playback->start("Damaged");
	healthbar_animation(old_health, healthbar);
}


void BattleCharacter::healthbar_animation(int old_health, TextureProgress* healthbar) {
	const Vector2 start_pos{538, 329};
	const Vector2 base_size{204, 61};
	float width = base_size.x * (static_cast<float>(old_health) / static_cast<float>(max_hp));
	float x = base_size.x * (static_cast<float>(hp) / static_cast<float>(max_hp));

	TextureRect* fragment = cast_to<TextureRect>(healthbar_fragment->instance());
	Ref<AtlasTexture> texture = fragment->get_texture()->duplicate();
	fragment->set_texture(texture);
	texture->set_region(Rect2(x, 0, width - x, base_size.y));
	fragment->set_position(start_pos + Vector2(x, 0));
	fragment->set_modulate(Color(1, 1, 1, 1));
	healthbar->get_node("../..")->add_child(fragment);

	Random* random = Random::get_singleton();
	
	Tween* tween = healthbar->get_node<Tween>("../../TweenFragment");
	tween->interpolate_property(fragment, "rect_position", fragment->get_position(), fragment->get_position() + Vector2(0, 400), 2.0f, Tween::TRANS_QUAD, Tween::EASE_IN);
	tween->interpolate_property(fragment, "rect_rotation", 0.0f, random->rand_range(-170.0f, -230.0f), random->rand_range(1.8f, 2.2f), Tween::TRANS_LINEAR, Tween::EASE_IN);
	tween->interpolate_property(fragment, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 1.5f, Tween::TRANS_LINEAR, Tween::EASE_IN_OUT, 2.0f);
	tween->connect("tween_all_completed", fragment, "queue_free", Array(), CONNECT_ONESHOT);
	tween->connect("tween_all_completed", healthbar, "hide", Array(), CONNECT_ONESHOT | CONNECT_REFERENCE_COUNTED);
	fragment->show();
	tween->start();
}

#undef GET_HEALTHBAR
