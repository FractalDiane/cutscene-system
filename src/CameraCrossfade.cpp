#include "CameraCrossfade.h"

#include "GDMacros.h"

#include <Viewport.hpp>
#include <Tween.hpp>
#include <SceneTree.hpp>

using namespace godot;

void CameraCrossfade::_register_methods() {
	REGISTER_METHOD(CameraCrossfade, _process);
	REGISTER_METHOD(CameraCrossfade, _on_TweenFade_tween_all_completed);
}

void CameraCrossfade::_init() {}


void CameraCrossfade::_process(float delta) {
	if (camera_clone)
		camera_clone->set_global_transform(old_camera_ref->get_global_transform());
}


void CameraCrossfade::crossfade(Camera* old_camera, Camera* new_camera, float time) {
	old_camera_ref = old_camera;
	Viewport* viewport = get_node<Viewport>("Viewport");
	viewport->set_size(cast_to<Viewport>(get_tree()->get_root())->get_size());
	Camera* camera = cast_to<Camera>(old_camera->duplicate());
	viewport->add_child(camera);
	camera_clone = camera;
	old_camera->set_current(false);
	new_camera->set_current(true);

	Tween* tween = get_node<Tween>("TweenFade");
	tween->interpolate_property(this, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), time);
	tween->start();
}


void CameraCrossfade::_on_TweenFade_tween_all_completed() {
	queue_free();
}
