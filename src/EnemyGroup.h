#pragma once

#include "GDMacros.h"

#include <Godot.hpp>
#include <Resource.hpp>

namespace godot {

class EnemyGroup : public Resource {
	GODOT_CLASS(EnemyGroup, Resource)

	Array enemies;
	Array enemy_positions;

public:
	static void _register_methods();
	void _init();

	GETTER(Array, enemies);
	GETTER(Array, enemy_positions);
};

}


