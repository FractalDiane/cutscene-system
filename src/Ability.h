#pragma once

#include "GDMacros.h"

#include <Godot.hpp>
#include <Resource.hpp>
#include <PackedScene.hpp>

namespace godot {

class Ability : public Resource {
	GODOT_CLASS(Ability, Resource)

protected:
	enum Type {
		ChangeHealth,
		ChangeStat,

	};

	String ability_name;
	int ability_element;
	int ability_type;
	int ability_cost;

	bool health_heal;
	int health_amount;
	
	int stat_affected_stat;

	Ref<PackedScene> visual_animation;

public:
	static void _register_methods();
	void _init();

	GETTER(String, ability_name);
	GETTER(int, ability_element);
	GETTER(int, ability_type);
	GETTER(int, ability_cost);

	GETTER_BOOL(health_heal);
	GETTER(int, health_amount);

	GETTER(int, stat_affected_stat);

	GETTER(Ref<PackedScene>, visual_animation);
};

}
