#pragma once

#include "BattleCharacter.h"
#include "Ability.h"
#include "BattleUI.h"

#include <Godot.hpp>
#include <Node.hpp>
#include <Camera.hpp>
#include <ResourceLoader.hpp>
#include <PackedScene.hpp>

#include <queue>

namespace godot {

class BattleTurn : public Node {
	GODOT_CLASS(BattleTurn, Node)

private:
	struct CharacterPriority {
		BattleCharacter* character;
		int priority;

		CharacterPriority(BattleCharacter* character, int priority) : character(character), priority(priority) {}
		bool operator<(const CharacterPriority& rvalue) const { return priority < rvalue.priority; }
	};

	static constexpr float CameraFadeTime = 0.5f;

	std::priority_queue<CharacterPriority> turn_queue;

	Array party;
	Array enemies;

	BattleCharacter* current_character;
	Camera* current_camera;
	BattleUI* battle_ui;

	Ref<PackedScene> particles_cast = ResourceLoader::get_singleton()->load("res://Prefabs/ParticlesSpellcast.tscn");
	Ref<PackedScene> particles_cast_2 = ResourceLoader::get_singleton()->load("res://Prefabs/ParticlesSpellcast2.tscn");
	Ref<PackedScene> camera_crossfade_ref = ResourceLoader::get_singleton()->load("res://Prefabs/CameraCrossfade.tscn");

public:
	static void _register_methods();
	void _init();

	void _ready();

	void initialize_turns(Array party, Array enemies);
	void run_next_turn();

	void use_ability(Ref<Ability> ability, Ref<PackedScene> animation, BattleCharacter* target);
	void damage(BattleCharacter* target, int amount);

	void fade_to_camera(Camera* target, bool stop_timer = false);
	void tween_to_camera(Camera* target, float time, bool stop_timer = false);

private:
	void show_cast_particles();

	void target_menu_opened(bool all_enemies);
	void target_menu_closed();
	void target_button_clicked(int index, int ability_index);

	void _on_TimerChangeCamera_timeout();
};

}
