#include "Dialogue.h"

#include "Controller.h"
#include "GDMacros.h"
#include "DialogueChoice.h"

#include <Input.hpp>
#include <SceneTree.hpp>
#include <Viewport.hpp>
#include <Label.hpp>
#include <Tween.hpp>

using namespace godot;

namespace {
	Input* input;
	Controller* controller;
}

void Dialogue::_register_methods() {
	REGISTER_METHOD(Dialogue, _ready);
	REGISTER_METHOD(Dialogue, _process);
	REGISTER_METHOD(Dialogue, choice_clicked);
	REGISTER_METHOD(Dialogue, _on_TweenAlpha_tween_all_completed);
	REGISTER_METHOD(Dialogue, _on_TimerText_timeout);

	REGISTER_PROPERTY_HINT(Dialogue, Ref<AudioStream>, text_sound, Ref<AudioStream>(), GODOT_PROPERTY_HINT_RESOURCE_TYPE, "AudioStream");

	register_signal<Dialogue>("text_closed");
	register_signal<Dialogue>("choice_selected", GODOT_VARIANT_TYPE_INT, "index");
}

void Dialogue::_init() {}


void Dialogue::_ready() {
	text = get_node<RichTextLabel>("CenterContainer/Box/Text");
	timer_text = get_node<Timer>("TimerText");

	input = Input::get_singleton();
	controller = Controller::get_singleton();

	get_node<Control>("CenterContainer")->hide();
}

void Dialogue::_process(float delta) {
	if (input->is_action_just_pressed("ui_accept")) {
		if (allow_advance) {
			allow_advance = false;
			finished = true;
			emit_signal("text_closed");
		}
		else if (text_active) {
			timer_text->stop();
			text->set_visible_characters(text->get_bbcode().length());
			text_active = false;

			if (choice_mode)
				show_choices();
			else
				allow_advance = true;
		}
	}
}


void Dialogue::display_text(String _text) {
	text->set_bbcode(_text);
	text->set_visible_characters(0);
	get_node<Control>("CenterContainer")->show();
	if (!choices.empty())
		choices.clear();
		
	choice_mode = false;
	finished = false;
	if (closed)
		fade_animation(false);
	else
		_on_TweenAlpha_tween_all_completed();
}


void Dialogue::display_choice(String _text, Array _choices) {
	text->set_bbcode(_text);
	text->set_visible_characters(0);
	get_node<Control>("CenterContainer")->show();
	choices = _choices;
	choice_mode = true;
	finished = false;
	if (closed)
		fade_animation(false);
	else
		_on_TweenAlpha_tween_all_completed();
}


void Dialogue::set_speaker(String speaker) {
	Control* box = get_node<Control>("CenterContainer/Box/Namebox");
	box->get_node<Label>("Text")->set_text(speaker);
	box->show();
}


void Dialogue::hide_speaker() {
	get_node<Control>("CenterContainer/Box/Namebox")->hide();
}

/*
void Dialogue::hide_box() {
	get_node<Control>("CenterContainer")->hide();
}
*/

void Dialogue::fade_animation(bool out) {
	if (!out) {
		closed = false;
	}
	else {
		if (closed)
			return;
		else
			closed = true;
	}

	Tween* tween = get_node<Tween>("TweenAlpha");
	tween->interpolate_property(get_node<Control>("CenterContainer/Box"), "modulate", out ? Color(1, 1, 1, 1) : Color(1, 1, 1, 0), out ? Color(1, 1, 1, 0) : Color(1, 1, 1, 1), 0.1f);
	tween->start();
}


void Dialogue::show_choices() {
	DialogueChoice* inst = cast_to<DialogueChoice>(choice_ref->instance());
	get_tree()->get_root()->add_child(inst);
	inst->connect("choice_selected", this, "choice_clicked");
	inst->create(choices);
}


void Dialogue::choice_clicked(int index) {
	emit_signal("choice_selected", index);
}


void Dialogue::_on_TweenAlpha_tween_all_completed() {
	if (!finished) {
		text_active = true;
		timer_text->start();
	}
}


void Dialogue::_on_TimerText_timeout() {
	controller->play_sound_oneshot(text_sound, -6, sound_pitch);
	int chars = text->get_visible_characters();
	text->set_visible_characters(chars + 1);
	if (chars + 1 >= text->get_bbcode().length()) {
		timer_text->stop();
		text_active = false;

		if (choice_mode)
			show_choices();
		else
			allow_advance = true;
	}
}
