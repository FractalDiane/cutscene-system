#pragma once

#include <Godot.hpp>
#include <Control.hpp>
#include <RichTextLabel.hpp>
#include <AudioStream.hpp>
#include <Timer.hpp>
#include <ResourceLoader.hpp>
#include <PackedScene.hpp>

#include "GDMacros.h"

namespace godot {

class Dialogue : public Control {
	GODOT_CLASS(Dialogue, Control)

private:
	Ref<AudioStream> text_sound;
	
	RichTextLabel* text;
	Timer* timer_text;

	float sound_pitch = 1.0f;

	Array choices{};
	bool choice_mode = false;

	bool allow_advance = false;
	bool text_active = false;
	bool finished = false;
	bool closed = true;

	Ref<PackedScene> choice_ref = ResourceLoader::get_singleton()->load("res://Prefabs/DialogueChoice.tscn");

public:
	static void _register_methods();

	void _init();
	void _ready();
	void _process(float delta);

	void display_text(String _text);
	void display_choice(String _text, Array _choices);
	void set_speaker(String speaker);

	SETTER(float, sound_pitch);

	void hide_speaker();
	//void hide_box();
	void fade_animation(bool out);

private:
	void show_choices();

	void choice_clicked(int index);
	void _on_TweenAlpha_tween_all_completed();
	void _on_TimerText_timeout();
};

}
