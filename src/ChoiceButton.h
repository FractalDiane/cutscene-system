#pragma once

#include <Godot.hpp>
#include <TextureButton.hpp>
#include <AudioStream.hpp>
#include <Tween.hpp>
#include <ShaderMaterial.hpp>

namespace godot {

class ChoiceButton : public TextureButton {
	GODOT_CLASS(ChoiceButton, TextureButton)

private:
	Ref<AudioStream> sound_hover;
	Ref<AudioStream> sound_click;

	bool auto_grab = false;
	bool button_ready = false;
	bool destroyed = false;
	Tween* tween_invert;
	Ref<ShaderMaterial> shader;

public:
	static void _register_methods();
	void _init();

	void _ready();

	void grab_focus_deferred();
	void set_choice_text(String text);
	void destroy();

private:
	void _on_focus_grabbed();
	void _on_focus_released();
	void _on_ChoiceButton_pressed();
	void _on_TweenInvert_tween_all_completed();
};

}
