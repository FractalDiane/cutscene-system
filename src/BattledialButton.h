#pragma once

#include <Godot.hpp>
#include <BaseButton.hpp>

#include "GDMacros.h"

namespace godot {

class BattledialButton : public BaseButton {
	GODOT_CLASS(BattledialButton, BaseButton)

private:
	//String command_name;

	int button_index;
	int index_offset;
	int target_index = 0;

public:
	static void _register_methods();
	void _init();

	void _ready();

	GETTER(int, target_index);
	SETTER(int, target_index);

private:
	void _on_focus_grabbed();
	void _on_focus_released();
	void _on_BattledialButton_pressed();
};

}
