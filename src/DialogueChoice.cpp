#include "DialogueChoice.h"

#include "ChoiceButton.h"
#include "GDMacros.h"

#include <Timer.hpp>
#include <Input.hpp>

using namespace godot;

void DialogueChoice::_register_methods() {
	REGISTER_METHOD(DialogueChoice, choice_clicked);
	REGISTER_METHOD(DialogueChoice, _on_TimerDestroy_timeout);

	register_signal<DialogueChoice>("choice_selected", "index", GODOT_VARIANT_TYPE_INT);
}

void DialogueChoice::_init() {}


void DialogueChoice::create(Array choices) {
	ChoiceButton* previous_button = nullptr;
	for (int i = 0; i < choices.size(); i++) {
		ChoiceButton* button = cast_to<ChoiceButton>(choice_button->instance());

		String choice = choices[i];
		button->set_choice_text(choice);

		if (i == 0 && !Input::get_singleton()->get_connected_joypads().empty())
			button->grab_focus_deferred();
		
		button->connect("button_pressed", this, "choice_clicked");
		get_node("Choices")->add_child(button);
		buttons.push_back(button);

		if (previous_button) {
			NodePath prev = previous_button->get_path();
			NodePath self = button->get_path();
			button->set_focus_previous(prev);
			button->set_focus_neighbour(GODOT_MARGIN_LEFT, prev);
			previous_button->set_focus_next(button->get_path());
			previous_button->set_focus_neighbour(GODOT_MARGIN_RIGHT, self);
		}

		previous_button = button;
	}
}


void DialogueChoice::choice_clicked(int index) {
	emit_signal("choice_selected", index);
	
	for (int i = 0; i < buttons.size(); i++)
		cast_to<ChoiceButton>(buttons[i])->destroy();

	get_node<Timer>("TimerDestroy")->start();
}


void DialogueChoice::_on_TimerDestroy_timeout() {
	queue_free();
}
