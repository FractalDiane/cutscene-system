#include "Cutscene.h"

#include "Controller.h"
#include "CameraCrossfade.h"
#include "GDMacros.h"
#include "YAML.h"

#include <Math.hpp>

#include <File.hpp>
#include <RegExMatch.hpp>
//#include <JSON.hpp>
//#include <JSONParseResult.hpp>
#include <SceneTree.hpp>
#include <Viewport.hpp>

#include <Camera.hpp>
#include <Tween.hpp>
#include <AnimationPlayer.hpp>
#include <AnimationTree.hpp>
#include <AnimationNodeStateMachinePlayback.hpp>

using namespace godot;

namespace {
	Controller* controller;
}

void Cutscene::_register_methods(){
	REGISTER_METHOD(Cutscene, _ready);
	REGISTER_METHOD(Cutscene, run_cutscene_step);
	REGISTER_METHOD(Cutscene, run_cutscene_step_choice);
	REGISTER_METHOD(Cutscene, _on_TimerWait_timeout);

	REGISTER_PROPERTY_HINT(Cutscene, String, cutscene_file, String(), GODOT_PROPERTY_HINT_FILE, "*.yaml,*.yml");
	REGISTER_PROPERTY(Cutscene, sounds_and_music, Dictionary());
	REGISTER_PROPERTY(Cutscene, autostart, false);
}

void Cutscene::_init() {}


void Cutscene::_ready() {
	controller = Controller::get_singleton();

	timer_wait = get_node<Timer>("TimerWait");
	dialogue = cast_to<Dialogue>(dialogue_ref->instance());
	get_tree()->get_root()->call_deferred("add_child", dialogue);

	command_regex->compile(CommandRegexPat);
	dialogue_regex->compile(DialogueRegexPat);
	choice_regex->compile(ChoiceRegexPat);
	function_regex->compile(FunctionRegexPat);

	if (autostart)
		start_cutscene();
}


void Cutscene::start_cutscene() {
	generate_cutscene_table(cutscene_file);
	next_step = "NodeStart";
	run_cutscene_step();
}


void Cutscene::generate_cutscene_table(String cutscene_file) {
	Ref<File> file = File::_new();
	file->open(cutscene_file, File::READ);
	cutscene_table = YAML::get_singleton()->decode(file->get_as_text());
	file->close();
}


void Cutscene::run_cutscene_step() {
	Dictionary current_step = cutscene_table[next_step];
	Dictionary step_outputs = current_step["outputs"];
	int step_type = command_map[current_step["type"]];

	if (step_type != Command::DialogueChoice) {
		String next_name = step_outputs[0];
		next_step = next_name;
	}

	Dictionary args = current_step["arguments"];
	bool close_dialogue = true;

	switch (step_type) {

		case Command::DialogueText: {
			String speaker = args["speaker_name"];
			float pitch = args["sound_pitch"];
			String text = args["text"];
			dialogue->connect("text_closed", this, "run_cutscene_step", Array(), CONNECT_ONESHOT | CONNECT_REFERENCE_COUNTED);
			if (speaker == "#")
				dialogue->hide_speaker();
			else
				dialogue->set_speaker(speaker);

			dialogue->set_sound_pitch(pitch);
			dialogue->display_text(text);
			close_dialogue = false;
		} break;

		case Command::DialogueChoice: {
			String speaker = args["speaker_name"];
			float pitch = args["sound_pitch"];
			String text = args["text"];
			Array choices = args["choices"];
			dialogue->connect("choice_selected", this, "run_cutscene_step_choice", Array(), CONNECT_ONESHOT | CONNECT_REFERENCE_COUNTED);
			if (speaker == "#")
				dialogue->hide_speaker();
			else
				dialogue->set_speaker(speaker);

			dialogue->set_sound_pitch(pitch);
			dialogue->display_choice(text, choices);
			close_dialogue = false;
		} break;

		case Command::CharacterAnimation: {
			String identifier = args["target_reference"];
			String animation = args["animation_name"];
			auto* tree = cast_to<Node>(declared_refs[identifier])->get_node<AnimationTree>("AnimationTree");
			AnimationNodeStateMachinePlayback* playback = tree->get("parameters/playback");
			playback->travel(animation);
			generic_wait(ShortWaitTime);
		} break;

		case Command::CharacterMove: {
			String identifier = args["target_reference"];
			Dictionary position = args["target_location"];
			float x = position["x"];
			float y = position["y"];
			float z = position["z"];
			float time = args["time"];
			Spatial* target = cast_to<Spatial>(declared_refs[identifier]);

			Tween* tween_move = controller->construct_tween();
			tween_move->interpolate_property(target, "translation", target->get_translation(), Vector3(x, y, z), time);
			tween_move->start();
			generic_wait(ShortWaitTime);
		} break;

		case Command::CharacterTurn: {
			String identifier = args["target_reference"];
			Dictionary rotation = args["target_rotation"];
			float x = rotation["x"];
			float z = rotation["z"];
			float time = args["time"];
			Spatial* target = cast_to<Spatial>(declared_refs[identifier]);
			Vector3 target_direction{x, target->get_translation().y, z};

			Vector3 start_rot = target->get_rotation();
			target->look_at(target_direction, Vector3(0, 1, 0));
			Vector3 lookat = target->get_rotation();
			target->set_rotation(start_rot);
			
			Tween* tween_turn = controller->construct_tween();
			tween_turn->interpolate_property(target, "rotation", start_rot, lookat, time);
			tween_turn->start();
			generic_wait(ShortWaitTime);
		} break;

		case Command::Fade: {
			float time = args["time"];
			String color = args["color"];
			bool fadein = args["fadein"];
			Controller::get_singleton()->fade(time, fadein, Color::hex((String("0x") + color).hex_to_int()));
			generic_wait(ShortWaitTime);
		} break;

		case Command::CameraFade: {
			String from = args["from_reference"];
			String to = args["to_reference"];
			float time = args["time"];
			Camera* cam_1 = cast_to<Camera>(declared_refs[from]);
			Camera* cam_2 = cast_to<Camera>(declared_refs[to]);

			if (time == 0.0f) {
				cam_1->set_current(false);
				cam_2->set_current(true);
			}
			else {
				CameraCrossfade* crossfade = cast_to<CameraCrossfade>(camera_crossfade_ref->instance());
				cam_2->get_parent()->add_child(crossfade);
				crossfade->crossfade(cam_1, cam_2, time);
			}

			generic_wait(ShortWaitTime);
		} break;

		case Command::CameraMove: {
			String identifier = args["target_reference"];
			Dictionary position = args["position"];
			Dictionary rotation = args["rotation"];
			float pos_x = position["x"];
			float pos_y = position["y"];
			float pos_z = position["z"];
			float rot_x = rotation["x"];
			float rot_y = rotation["y"];
			float rot_z = rotation["z"];
			float time = args["time"];
			bool interp = args["interpolation"];
			Camera* camera = cast_to<Camera>(declared_refs[identifier]);
			
			Transform xform = Transform();
			xform.translate(Vector3(pos_x, pos_y, pos_z));
			xform.rotate_basis(Vector3(1, 0, 0), Math::deg2rad(rot_x));
			xform.rotate_basis(Vector3(0, 1, 0), Math::deg2rad(rot_y));
			xform.rotate_basis(Vector3(0, 0, 1), Math::deg2rad(rot_z));
			xform.orthonormalize();

			Tween* tween_camera = controller->construct_tween();
			tween_camera->interpolate_property(camera, "transform", camera->get_transform(), xform, time, interp ? Tween::TRANS_QUINT : Tween::TRANS_LINEAR, Tween::EASE_IN_OUT);
			tween_camera->start();
			generic_wait(ShortWaitTime);
		} break;

		case Command::PlaySound: {
			String sound = args["sound_name"];
			float volume = args["volume"];
			float pitch = args["pitch"];
			Controller::get_singleton()->play_sound_oneshot(sounds_and_music[sound], volume, pitch);
			generic_wait(ShortWaitTime);
		} break;

		case Command::PlayMusic: {
			String music = args["music_name"];
			float volume = args["volume"];
			float pitch = args["pitch"];
			Controller::get_singleton()->play_music(sounds_and_music[music], volume, pitch);
			generic_wait(ShortWaitTime);
		} break;

		case Command::FadeMusic: {
			float time = args["time"];
			bool fadein = args["fadein"];
			Controller::get_singleton()->fade_music(time, fadein);
			generic_wait(ShortWaitTime);
		} break;

		case Command::DeclareRef: {
			String identifier = args["reference_name"];
			String path = args["target"];
			declared_refs[identifier] = get_node(NodePath("/root/Scene/" + path));
			generic_wait(ShortWaitTime);
		} break;

		case Command::PlayEventAnimation: {
			String name = args["animation_name"];
			get_node<AnimationPlayer>("AnimationPlayer")->play(name);
			generic_wait(ShortWaitTime);
		} break;

		case Command::SetFlag: {
			String name = args["flag"];
			int value = args["value"];
			Controller::get_singleton()->set_flag(name, value);
			generic_wait(ShortWaitTime);
		} break;

		case Command::SetProperty: {
			String identifier = args["target_reference"];
			String property = args["property"];
			Variant value = args["value"];
			cast_to<Node>(declared_refs[identifier])->set(property, value);
			generic_wait(ShortWaitTime);
		} break;

		case Command::CallFunction: {
			String identifier = args["target_reference"];
			String function = args["function"];
			String func_args = args["args"];
		} break;

		case Command::Wait: {
			float time = args["time"];
			generic_wait(time);
		} break;

		case Command::Start: {
			//Godot::print("CUTSCENE START");
			generic_wait(ShortWaitTime);
		} break;

		case Command::End: {
			//Godot::print("CUTSCENE DONE");
		} break;

#ifdef GDN_DEBUG
		default: {
			PRINT_ERROR(String("Parsed an invalid cutscene command ({0})").format(Array::make(current_step["type"])));
		} break;
#endif
	}

	if (close_dialogue)
		dialogue->fade_animation(true);
}


void Cutscene::run_cutscene_step_choice(int choice_index) {
	Dictionary current_step = cutscene_table[next_step];
	Dictionary step_outputs = current_step["outputs"];
	String next_name = step_outputs[choice_index];
	next_step = next_name;

	run_cutscene_step();
}


void Cutscene::generic_wait(float time) {
	timer_wait->set_wait_time(time);
	timer_wait->start();
}


void Cutscene::_on_TimerWait_timeout() {
	run_cutscene_step();
}
