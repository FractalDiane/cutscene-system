#include "Random.h"

#include "GDMacros.h"

using namespace godot;

Random* Random::singleton = nullptr;

void Random::_register_methods() {
	REGISTER_METHOD(Random, _ready);
}

void Random::_init() {}


void Random::_ready() {
	if (!singleton)
		singleton = this;
	else {
		queue_free();
		return;
	}
	
	_random->randomize();
}


int Random::rand_range(int from, int to) {
	return _random->randi_range(from, to);
}


float Random::rand_range(float from, float to) {
	return _random->randf_range(from, to);
}
