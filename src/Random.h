#pragma once

#include "GDMacros.h"

#include <Godot.hpp>
#include <Node.hpp>
#include <RandomNumberGenerator.hpp>

namespace godot {

class Random : public Node {
	GODOT_CLASS(Random, Node)

private:
	static Random* singleton;
	Ref<RandomNumberGenerator> _random = RandomNumberGenerator::_new();

public:
	static void _register_methods();
	void _init();

	void _ready();

	int rand_range(int from, int to);
	float rand_range(float from, float to);
	
	__ALWAYS_INLINE__ bool random_chance(float probability) { return rand_range(0.0f, 1.0f) < probability; }
	__ALWAYS_INLINE__ Variant random_item(Array array) { return array[rand_range(0, array.size() - 1)]; }

	template <typename T, typename... Args>
	T choose(T first, Args... args) {
		T array[] = {first, args...};
		return array[rand_range(0, sizeof...(Args))];
	}
	
	static inline Random* get_singleton() { return singleton; }
};

}
