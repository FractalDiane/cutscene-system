#include "Controller.h"

#include "GDMacros.h"

#include <SceneTree.hpp>
#include <Viewport.hpp>
#include <Input.hpp>
#include <OS.hpp>

#include <ColorRect.hpp>
#include <AudioStreamPlayer.hpp>

using namespace godot;

namespace {
	Input* input;
}

Controller* Controller::singleton = nullptr;

void Controller::_register_methods() {
	REGISTER_METHOD(Controller, _ready);
	REGISTER_METHOD(Controller, _process);
}

void Controller::_init() {}


void Controller::_ready() {
	if (!singleton)
		singleton = this;
	else
		queue_free();

	input = Input::get_singleton();
}

void Controller::_process(float delta) {
	if (input->is_action_just_pressed("sys_fullscreen")) {
		OS* os = OS::get_singleton();
		os->set_window_fullscreen(!os->is_window_fullscreen());
	}
}


void Controller::fade(float time, bool fadein, Color color) {
	Color start = fadein ? Color(color.r, color.g, color.b, 1) : Color(color.r, color.g, color.b, 0);
	Color end = fadein ? Color(color.r, color.g, color.b, 0) : Color(color.r, color.g, color.b, 1);

	Tween* tween = get_node<Tween>("TweenFade");
	tween->interpolate_property(get_node<ColorRect>("CanvasLayer/Fade"), "color", start, end, time);
	tween->start();
}


int Controller::flag(String name) {
	return flags[name];
}


void Controller::set_flag(String name, int value) {
	flags[name] = value;
}


void Controller::play_sound_oneshot(Ref<AudioStream> sound, float volume, float pitch) {
	AudioStreamPlayer* oneshot = AudioStreamPlayer::_new();
	oneshot->connect("finished", oneshot, "queue_free");
	oneshot->set_stream(sound);;
	oneshot->set_volume_db(volume);
	oneshot->set_pitch_scale(pitch);
	get_tree()->get_root()->add_child(oneshot);
	oneshot->play();
}


void Controller::play_music(Ref<AudioStream> _music, float volume, float pitch) {
	AudioStreamPlayer* music = get_node<AudioStreamPlayer>("Music");
	music->set_stream(_music);
	music->set_volume_db(volume);
	music->set_pitch_scale(pitch);
	music->play();
}


void Controller::fade_music(float time, bool fadein) {
	Tween* tween = get_node<Tween>("TweenMusic");
	AudioStreamPlayer* music = get_node<AudioStreamPlayer>("Music");

	tween->interpolate_property(music, "volume_db", fadein ? -60.0f : 0.0f, fadein ? 0.0f : -60.0f, time);
	tween->start();
}


Tween* Controller::construct_tween(bool cleanup_safeguard, float safeguard_time) {
	Tween* tween = Tween::_new();
	tween->connect("tween_all_completed", tween, "queue_free", Array(), CONNECT_ONESHOT);

	if (cleanup_safeguard) {
		Timer* timer = construct_timer(safeguard_time, tween, "queue_free");
		tween->connect("tween_all_completed", timer, "queue_free", Array(), CONNECT_ONESHOT);
	}
	
	get_tree()->get_root()->add_child(tween);
	return tween;
}


Timer* Controller::construct_timer(float time, const Object* target, String method, Array binds) {
	Timer* timer = Timer::_new();
	timer->set_wait_time(time);
	timer->set_one_shot(true);
	timer->connect("timeout", target, method, binds, CONNECT_ONESHOT | CONNECT_REFERENCE_COUNTED);
	timer->connect("timeout", timer, "queue_free", Array(), CONNECT_ONESHOT);
	get_tree()->get_root()->add_child(timer);
	timer->start();
	return timer;
}
