#pragma once

#include "Character.h"
#include "EnemyGroup.h"
#include "BattleTurn.h"

#include <Godot.hpp>
#include <Spatial.hpp>

namespace godot {

class Battle : public Spatial {
	GODOT_CLASS(Battle, Spatial)

private:
	static constexpr const char* HealthbarPath = "res://Prefabs/Healthbar3D.tscn";

	Ref<EnemyGroup> enemy_group;

	Array party{};
	Array enemies{};

	BattleTurn* turn_manager;

	GETTER(Ref<EnemyGroup>, enemy_group);
	SETTER(Ref<EnemyGroup>, enemy_group);

public:
	static void _register_methods();
	void _init();

	void _ready();

private:
	void spawn_party(Ref<PackedScene> healthbar);
	void spawn_enemies(Ref<PackedScene> healthbar);
	void spawn_character(Ref<Character> data, Vector3 position, Ref<PackedScene> healthbar, Array char_array);
	void start_battle();
	void new_turn();
};

}
